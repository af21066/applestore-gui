import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;

public class main {
    private JPanel root;
    private JLabel topLabel;
    private JButton sunfujiButton;
    private JButton hirosakifujiButton;
    private JButton jonagoldButton;
    private JTextPane receivedinfo;
    private JComboBox pulldown;
    private JButton shinanodolceButton;
    private JButton shinanoSweetButton;
    private JButton tsugaruButton;
    private JTextPane orderedItemsNameList;
    private JTextPane orderedItemsPriceList;
    private JTextPane totalPrice;
    private JLabel title;
    private JLabel sunfujiprice;
    private JLabel hirosakiprice;
    private JLabel jonagoldprice;
    private JLabel shinanodolceprice;
    private JLabel shinanosweetprice;
    private JLabel tsugaruprice;
    private JLabel orderitem;
    private JButton chekout;
    private JLabel totalpricelabel;
    private JButton exitButton;
    List<Integer> totalPriceList = new ArrayList<Integer>();
    String language = "En";
    int taxPer = 10;
    Map<String, String> sunfujiMap = Map.of(
            "nameJa","サンふじ","nameEn","Sun fuji","nameCh","无袋富士"
    );
    Map<String, String> hirosakifujiMap = Map.of(
            "nameJa","ひろさきふじ","nameEn","Hirosaki fuji","nameCh","弘前富士"
    );
    Map<String, String> jonagoldMap = Map.of(
            "nameJa","ジョンゴールド","nameEn","Jonagold","nameCh","乔纳金"
    );
    Map<String, String> shinanodolceMap = Map.of(
            "nameJa","シナノドルチェ","nameEn","shinanodolce","nameCh","信浓甜蜜"
    );
    Map<String, String> shinanosweetMap = Map.of(
            "nameJa","シナノスイート","nameEn","Shinano Sweet","nameCh","信浓甜甜"
    );
    Map<String, String> tsugaruMap = Map.of(
            "nameJa","つがる","nameEn","Tsugaru","nameCh","津轻"
    );
    Map<String, String> topLabelMap = Map.of(
            "topLabelJa","注文を選択してください",
            "topLabelEn","Please select your order",
            "topLabelCh","请选择您的订单"
    );

    void order(String foodName,int price){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order "+foodName+"?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == 0){
            JOptionPane.showMessageDialog(null,"Order for "+foodName+" received");
            receivedinfo.setText("Order for "+foodName+" received");
            totalPriceList.add(price);
            addOrderedList(foodName,price);
            totalPrice.setText("￥ "+ totalpricecal(totalPriceList) + "\n");
        }
    }
    void addOrderedList(String foodName,int price){
        String currentNameText = orderedItemsNameList.getText();
        String currentPriceText = orderedItemsPriceList.getText();
        orderedItemsNameList.setText(currentNameText + foodName + "\n");
        orderedItemsPriceList.setText(currentPriceText + "￥ " +price + "\n");
    }
    int totalpricecal(List<Integer> totalprice){
        int sum = totalprice.stream().mapToInt(i -> i).sum();
        int sumTax = sum*(100+taxPer)/100;
        return sum;

    }

    //言語を変更する
    void changLanguage(){
            sunfujiButton.setText(sunfujiMap.get("name"+language));
            hirosakifujiButton.setText(hirosakifujiMap.get("name"+language));
            jonagoldButton.setText(jonagoldMap.get("name"+language));
            shinanodolceButton.setText(shinanodolceMap.get("name"+language));
            shinanoSweetButton.setText(shinanosweetMap.get("name"+language));
            tsugaruButton.setText(tsugaruMap.get("name"+language));
            topLabel.setText(topLabelMap.get("topLabel"+language));
    }

    public main() {
        sunfujiButton.setIcon(new ImageIcon(this.getClass().getResource("sanfuji.jpg")));
        sunfujiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Sunfuji",310);
            }
        });
        hirosakifujiButton.setIcon(new ImageIcon(this.getClass().getResource("hirosakifuji.jpg")));
        hirosakifujiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Hirosakifuji",320);
            }
        });
        jonagoldButton.setIcon(new ImageIcon(this.getClass().getResource("jonagold.jpg")));
        jonagoldButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Jonagold",280);
            }
        });
        shinanodolceButton.setIcon(new ImageIcon(this.getClass().getResource("shinanoDolce.jpg")));
        shinanodolceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Shinano Dolce",300);
            }
        });
        shinanoSweetButton.setIcon(new ImageIcon(this.getClass().getResource("shinanoSweet.jpg")));
        shinanoSweetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Shinano Sweet",290);
            }
        });
        tsugaruButton.setIcon(new ImageIcon(this.getClass().getResource("tsugaru.png")));
        tsugaruButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tsugaru",305);
            }
        });
        pulldown.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String selectedOption = (String) pulldown.getSelectedItem();
                if(selectedOption.equals("English")){
                    language = "En";
                }
                else if(selectedOption.equals("日本語")){
                    language = "Ja";
                }
                else if(selectedOption.equals("中文")){
                    language = "Ch";
                }
                changLanguage();
            }
        });
        chekout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0){
                    int sum = totalPriceList.stream().mapToInt(i -> i).sum();
                    JOptionPane.showMessageDialog(null,"Thank you. The total price is "+sum+"yen");
                    orderedItemsNameList.setText("");
                    orderedItemsPriceList.setText("");
                    receivedinfo.setText("");
                    totalPriceList.clear();
                    totalPrice.setText("￥ "+ totalpricecal(totalPriceList) + "\n");
                }
            }
        });
        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Do you really want to exit?",
                        "Exit Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == JOptionPane.YES_OPTION) {
                    System.exit(0);
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("SimpleFoodOrderingMachine");
        frame.setContentPane(new main().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
//    private void createUIComponents() {
//        // TODO: place custom component creation code here
//    }
}